import React, {Component} from 'react';
import './App.css';

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function Square(props) {
    return(
      <button 
        className='square'
        onClick={props.onClick}
      >
        {props.value}
      </button>
    )
}


class Board extends Component{
  renderSquare(i){
    return (
      <Square 
      value={this.props.squares[i]}
      onClick={() => this.props.onClick(i)}
      />
    )
  }
  
  render(){
    return(
      <>
        <div className='board-row'>
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className='board-row'>
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className='board-row'>
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </>
    )
  }
}

class Game extends Component{
  constructor(props){
    super(props)
    this.state = {
      history:[{
        squares: Array(9).fill(null)
      }],
      xIsNext: true,
      stepNumber: 0
    }
  }

  handleClick = (i) => {
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const currentGameState = history[history.length - 1]
    const squares = currentGameState.squares.slice(); // Immutability
    if (calculateWinner(squares) || squares[i]){
      return
    }
    squares[i] = this.state.xIsNext ? 'X': 'O'

    this.setState({
      xIsNext: !this.state.xIsNext,
      history: history.concat([{squares}]),      
      stepNumber: history.length
    })
  }

  jumpTo = (step) => {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    })
  }

  render(){
    const history = this.state.history
    const currentGameState = history[this.state.stepNumber]
    console.log(history, this.state.stepNumber)
    const winner = calculateWinner(currentGameState.squares)
    let status;
    if (winner) {
      status = 'Winner: ' + winner
    }
    else{
      status = 'Next Player: ' + (this.state.xIsNext ? 'X' : 'O')
    }

    const moves = history.map((step, move) => {
      const desc = move ? 
        'Go to step ' + move :
        'Go to game start'
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    })
    
    return(
      <div className='game'>
      <div className='game-board'>
        <Board 
          squares={currentGameState.squares}
          onClick={(i) => this.handleClick(i)}
        />
      </div>
      <div className='game-info'>
          <div>{status}</div>
          <ul>{moves}</ul>
      </div>
      </div>
    );
  }
}

class App extends Component {
  render(){
    return(
      <Game/>
    )
  }
}

export default App;
